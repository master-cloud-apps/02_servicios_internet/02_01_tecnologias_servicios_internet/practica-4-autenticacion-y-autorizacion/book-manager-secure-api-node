const { getFullUrl, checkMongoId, checkModelNone, manageChainError, createModel } = require('./../utils/index.js')
const Comment = require('./../../models/comment.js')
const Book = require('./../../models/book.js')
const User = require('./../../models/user.js')

const handlers = () => ({
  post: (req, res) => {
    const createComment = (user) => new Comment({
      content: req.body.content,
      punctuation: req.body.punctuation,
      book: req.params.bookId,
      user: user._id
    })
    const getUserNickFromRequest = () => {
      return User.findOne({ nick: req.body.nick })
        .then(user => checkModelNone(user, res))
    }

    const checkBook = () => {
      return Promise.resolve(checkMongoId(req.params.bookId, res))
        .then(bookId => Book.findById(bookId))
        .then(book => checkModelNone(book, res))
    }

    return checkBook()
      .then(book => getUserNickFromRequest())
      .then(user => createComment(user))
      .then(comment => createModel(comment, res))
      .then(comment => res.status(201).location(getFullUrl(req) + comment.id).send())
      .catch(error => manageChainError(error, res))
  },
  getById: (req, res) => {
    const checkCommentNone = comment => checkModelNone(comment, res)

    return Promise.resolve(checkMongoId(req.params.bookId, res))
      .then(bookId => Book.findById(bookId))
      .then(book => checkModelNone(book, res))
      .then(() => checkMongoId(req.params.commentId, res))
      .then(id => Comment.findById(id).populate('book').populate('user'))
      .then(comment => checkCommentNone(comment))
      .then(comment => comment.toJSON())
      .then(mappedComment => res.status(200).send(mappedComment))
      .catch(error => manageChainError(error, res))
  }
})

module.exports = handlers

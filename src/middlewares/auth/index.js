const jwt = require('jsonwebtoken')

const REGISTERED = 'REGISTERED'
const NOT_REGISTERED = 'NOT_REGISTERED'

const getToken = authHeader => {
  if (authHeader) {
    return authHeader.replace('Bearer ', '')
  }
  return undefined
}

const setRole = (req, res, next) => {
  const token = getToken(req.header('Authorization'))
  if (token === undefined) {
    req.role = NOT_REGISTERED
    return next()
  }
  try {
    jwt.verify(token, process.env.JWT_SECRET)
    req.token = token
    req.role = REGISTERED
    next()
  } catch {
    req.role = NOT_REGISTERED
    next()
  }
}

const verify = (req, res, next) => {
  const token = getToken(req.header('Authorization'))
  if (token === undefined) {
    return res.status(401).send({ error: 'Bad authentication' })
  }
  try {
    jwt.verify(token, process.env.JWT_SECRET)
    req.token = token
    next()
  } catch {
    return res.status(401).send({ error: 'Bad authentication' })
  }
}

module.exports = {
  setRole,
  REGISTERED,
  NOT_REGISTERED,
  verify
}

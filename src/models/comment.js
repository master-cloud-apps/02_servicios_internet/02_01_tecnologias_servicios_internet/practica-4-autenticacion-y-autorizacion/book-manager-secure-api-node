const mongoose = require('mongoose')

const commentSchema = new mongoose.Schema({
  content: { type: String, required: true },
  punctuation: { type: Number, required: true },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  book: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Book'
  }
})

commentSchema.methods.toJSON = function() {
  const commentObject = this.toObject()

  commentObject.id = commentObject._id

  delete commentObject._id
  delete commentObject.__v

  return commentObject
}

commentSchema.methods.toJSONNoFK = function() {
  const commentObject = this.toObject()

  commentObject.id = commentObject._id

  delete commentObject._id
  delete commentObject.__v
  delete commentObject.user
  delete commentObject.book

  return commentObject
}

module.exports = mongoose.model('Comment', commentSchema)

const express = require('express')
const { books } = require('./../endpoints/index.js')
const { setRole, verify } = require('../middlewares/auth/index.js')

const router = new express.Router()
const bookHandler = books()

router.post('/books', verify, bookHandler.post)
router.get('/books', setRole, bookHandler.get)
router.get('/books/:bookId', verify, bookHandler.getById)
router.delete('/books/:bookId', verify, bookHandler.deleteById)
router.put('/books/:bookId', verify, bookHandler.updateById)

module.exports = router

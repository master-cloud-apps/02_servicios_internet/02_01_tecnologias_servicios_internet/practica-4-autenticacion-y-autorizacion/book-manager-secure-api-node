const { connect } = require('./src/db/mongoose.js')
const createServer = require('./src/server/index.js')

connect(process.env.DATABASE_URL)
  .then(_ => createServer().listen(3000, () => console.log('Serving in port 3000')))
  .catch(error => console.log(error))

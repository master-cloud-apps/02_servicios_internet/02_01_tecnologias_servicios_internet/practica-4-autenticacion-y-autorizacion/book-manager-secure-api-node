const request = require('supertest')

const bookTest = {
  title: 'Test title',
  yearPublication: 1974,
  editorial: 'Test editorial',
  review: 'This is a test book, what do you want?',
  author: 'Alberto Eyo'
}
const createBook = ({ book, app, token = undefined }) =>
  request(app)
    .post('/books')
    .set('Authorization', `Bearer ${token}`)
    .send(book)
const deleteBookById = ({ bookId, app, token = undefined }) =>
  request(app)
    .delete(`/books/${bookId}`)
    .set('Authorization', `Bearer ${token}`)
const updateBookById = ({ bookId, app, token = undefined, book }) =>
  request(app)
    .put(`/books/${bookId}`)
    .set('Authorization', `Bearer ${token}`)
    .send(book)
const getBookById = ({ bookId, app, token = undefined }) =>
  request(app)
    .get(`/books/${bookId}`)
    .set('Authorization', `Bearer ${token}`)
const createTestBook = (app, token = undefined) => createBook({ book: bookTest, app, token })

module.exports = {
  bookTest, createBook, createTestBook, deleteBookById, getBookById, updateBookById
}

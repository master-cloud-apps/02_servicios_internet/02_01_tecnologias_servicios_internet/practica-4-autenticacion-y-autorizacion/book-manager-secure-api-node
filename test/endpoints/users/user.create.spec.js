const request = require('supertest')
const app = require('./../../../src/app/index.js')
const { manageInMemoryDatabase } = require('./../index.js')
const { testUser } = require('./../user.js')
const { expect } = require('chai')

describe('User use cases', () => {
  manageInMemoryDatabase()

  describe('Create User Entity use cases', () => {
    it('When create user correctly should return 201', async () => {
      const response = await request(app).post('/users').send(testUser)

      expect(response.statusCode).to.be.equal(201)
    })
    it('When create user should get location header', async () => {
      const responseCreateUser = await request(app)
        .post('/users')
        .send(testUser)

      expect(responseCreateUser.headers.location).to.not.be.equal(undefined)
    })
    it('When create user should get authorization header', async () => {
      const responseCreateUser = await request(app)
        .post('/users')
        .send(testUser)

      expect(responseCreateUser.headers.authorization).to.not.be.equal(undefined)
    })
    it('Given user created, create user same nick should return bad request', async () => {
      const responseCreateUser = await request(app)
        .post('/users')
        .send(testUser)
      expect(responseCreateUser.statusCode).to.be.equal(201)

      const response = await request(app).post('/users').send(testUser)

      expect(response.statusCode).to.be.equal(400)
    })
  })
})

const app = require('./../../../src/app/index.js')
const { manageInMemoryDatabase, getIdFromResponseLocationHeader } = require('./../index.js')
const { createTestUserAndGetId, getUserById, getUsers, getUserComments, deleteUserById } = require('./../user.js')
const { createTestBook } = require('./../book.js')
const { expect } = require('chai')
const { createTestComment } = require('../comment.js')
const { setupDatabase, userOne } = require('../../fixtures/db.js')

describe('User use cases', () => {
  manageInMemoryDatabase()
  beforeEach(setupDatabase)

  const token = userOne.tokens[0].token
  describe('Get user by id use cases. Given user created', () => {
    it('When get user by id on existing user should return status code 200', () => {
      return createTestUserAndGetId(app)
        .then(userId => getUserById({ app, userId, token }))
        .then(response => expect(response.statusCode).to.equal(200))
    })
    it('When get user by id on existing user should return user', () => {
      return createTestUserAndGetId(app)
        .then(userId => getUserById({ app, userId, token }))
        .then(response => expect(response.body).to.deep.include({ nick: 'test_nick' }))
    })
    it('When get user by id on existing user should return user no password', () => {
      return createTestUserAndGetId(app)
        .then(userId => getUserById({ app, userId, token }))
        .then(response => expect(response.body.password).to.be.equal(undefined))
    })
    it('When get user by id on existing user should return user no tokens', () => {
      return createTestUserAndGetId(app)
        .then(userId => getUserById({ app, userId, token }))
        .then(response => expect(response.body.tokens).to.be.equal(undefined))
    })
    it('When get user by id wrong should return bad request', () => {
      return createTestUserAndGetId(app)
        .then(_ => getUserById({ app, userId: 12345, token }))
        .then(response => expect(response.status).to.be.equal(400))
    })
    it('Given created user when delete user then get user by id should return 404', () => {
      let userTestId
      return createTestUserAndGetId(app)
        .then(userId => (userTestId = userId))
        .then(() => getUserById({ app, userId: userTestId, token }))
        .then(response => expect(response.statusCode).to.be.equal(200))
        .then(() => deleteUserById({ app, token, userId: userTestId }))
        .then(response => expect(response.statusCode).to.be.equal(204))
        .then(() => getUserById({ app, userId: userTestId, token }))
        .then(response => expect(response.statusCode).to.be.equal(404))
    })
  })
  describe('Get users use cases', () => {
    it('Given no user created, should return ok', () => {
      return getUsers({ app, token })
        .then(response => expect(response.statusCode).to.be.equal(200))
    })
    it('Given no user created, should return empty list', () => {
      return getUsers({ app, token })
        .then(response => expect(response.body.length).to.be.equal(1))
    })
    it('Given one user created, should return list size 1', () => {
      return createTestUserAndGetId(app)
        .then(() => getUsers({ app, token }))
        .then(response => expect(response.body.length).to.be.equal(2))
    })
    it('Given one user created, when get users should not show passwords', () => {
      return createTestUserAndGetId(app)
        .then(() => getUsers({ app, token }))
        .then(response => expect(response.body[0].password).to.be.equal(undefined))
    })
    it('Given one user created, when get users should not show tokens', () => {
      return createTestUserAndGetId(app)
        .then(() => getUsers({ app, token }))
        .then(response => expect(response.body[0].tokens).to.be.equal(undefined))
    })
  })
  describe('Get user comments use cases', () => {
    let testBookId
    let user
    beforeEach(() => createTestBook(app, userOne.tokens[0].token)
      .then(response => getIdFromResponseLocationHeader(response))
      .then(bookId => (testBookId = bookId))
      .then(() => createTestUserAndGetId(app))
      .then(userId => getUserById({ userId, app, token }))
      .then(response => (user = response.body)))
    describe('Given usercreated', () => {
      it('Should get all comments from user', () => {
        return getUserComments({ app, userId: user.id, token })
          .then(response => {
            expect(response.statusCode).to.be.equal(200)
            expect(response.body.length).to.be.equal(0)
          })
      })
      it('When get comments form invalid user id should return bad request', () => {
        return getUserComments({ app, userId: 1, token })
          .then(response => {
            expect(response.statusCode).to.be.equal(400)
          })
      })
    })

    describe('Given user and book created', () => {
      it('Should get all comments from user', () => {
        let commenTestId
        return createTestComment({ app, bookId: testBookId, nick: user.nick, token })
          .then(response => {
            commenTestId = getIdFromResponseLocationHeader(response)
            return getUserComments({ app, userId: user.id, token })
          })
          .then(response => {
            expect(response.statusCode).to.be.equal(200)
            expect(response.body.length).to.be.equal(1)
            expect(response.body[0].id).to.be.equal(commenTestId)
          })
      })
      it('Should get all comments from user (2 comments)', () => {
        const createCommentOverBookAndUser = () => createTestComment({ app, bookId: testBookId, nick: user.nick, token })
        return Promise.all([createCommentOverBookAndUser(), createCommentOverBookAndUser()])
          .then(() => getUserComments({ app, userId: user.id, token }))
          .then(response => {
            expect(response.statusCode).to.be.equal(200)
            expect(response.body.length).to.be.equal(2)
          })
      })
    })
  })
})

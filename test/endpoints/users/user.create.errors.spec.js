const request = require('supertest')
const app = require('../../../src/app/index.js')
const { manageInMemoryDatabase } = require('../index.js')
const { testUser } = require('../user.js')
const { expect } = require('chai')
const _ = require('lodash')

describe('User use cases', () => {
  manageInMemoryDatabase()

  describe('Create User Entity use cases', () => {
    it('When create user no password should return error', async () => {
      const userErrorNoPass = _.cloneDeep(testUser)
      delete userErrorNoPass.password
      const response = await request(app).post('/users').send(userErrorNoPass)

      expect(response.statusCode).to.be.equal(400)
    })
    it('When create user password invalid should return error', async () => {
      const userErrorWrongPass = _.cloneDeep(testUser)
      userErrorWrongPass.password = 'passwordNotValid'
      const response = await request(app).post('/users').send(userErrorWrongPass)

      expect(response.statusCode).to.be.equal(400)
    })
    it('Given password short When create user should return error', async () => {
      const userErrorWrongPass = _.cloneDeep(testUser)
      userErrorWrongPass.password = '123'
      const response = await request(app).post('/users').send(userErrorWrongPass)

      expect(response.statusCode).to.be.equal(400)
    })
    it('When create user no nick should return error', async () => {
      const userErrorNoNick = _.cloneDeep(testUser)
      delete userErrorNoNick.nick
      const response = await request(app).post('/users').send(userErrorNoNick)

      expect(response.statusCode).to.be.equal(400)
    })
  })
})

const app = require('./../../../src/app/index.js')
const { manageInMemoryDatabase, getIdFromResponseLocationHeader } = require('./../index.js')
const { createTestUserAndGetId, getUserById, deleteUserById } = require('./../user.js')
const { createTestComment, getCommentById } = require('./../comment.js')
const { createTestBook } = require('./../book.js')
const { setupDatabase, userOne } = require('../../fixtures/db.js')
const { expect } = require('chai')

describe('User use cases', () => {
  manageInMemoryDatabase()
  beforeEach(setupDatabase)

  const token = userOne.tokens[0].token
  describe('DELETE /users/:userId use cases', () => {
    it('Given user created, when delete by id should return 204', () => {
      return createTestUserAndGetId(app)
        .then(userId => deleteUserById({ app, userId, token }))
        .then(response => expect(response.statusCode).to.equal(204))
    })
    it('Given user created, when delete by id wrong id should return bad request', () => {
      return deleteUserById({ app, userId: 12345, token })
        .then(response => expect(response.statusCode).to.equal(400))
    })
    it('Given created user when delete user then get user by id should return 404', () => {
      let userTestId
      return createTestUserAndGetId(app)
        .then(userId => (userTestId = userId))
        .then(() => getUserById({ app, token, userId: userTestId }))
        .then(response => expect(response.statusCode).to.be.equal(200))
        .then(() => deleteUserById({ app, userId: userTestId, token }))
        .then(response => expect(response.statusCode).to.be.equal(204))
        .then(() => getUserById({ app, token, userId: userTestId }))
        .then(response => expect(response.statusCode).to.be.equal(404))
    })
    it('Given created user when delete user twice then second time should return 404', () => {
      let userTestId
      return createTestUserAndGetId(app)
        .then(userId => (userTestId = userId))
        .then(() => getUserById({ app, token, userId: userTestId }))
        .then(response => expect(response.statusCode).to.be.equal(200))
        .then(() => deleteUserById({ app, userId: userTestId, token }))
        .then(response => expect(response.statusCode).to.be.equal(204))
        .then(() => deleteUserById({ app, userId: userTestId, token }))
        .then(response => expect(response.statusCode).to.be.equal(404))
    })
    describe('Given user, book and comment created', () => {
      let testBookId
      let user
      beforeEach(() => createTestBook(app, userOne.tokens[0].token)
        .then(response => getIdFromResponseLocationHeader(response))
        .then(bookId => (testBookId = bookId))
        .then(() => createTestUserAndGetId(app))
        .then(userId => getUserById({ userId, app, token }))
        .then(response => (user = response.body))
        .then(() => createTestComment({ app, nick: user.nick, bookId: testBookId, token }))
        .then(response => getIdFromResponseLocationHeader(response))
        .then(commentId => getCommentById({ bookId: testBookId, commentId, app, token })))
      it('When delete should return bad request', () => {
        return deleteUserById({ app, userId: user.id, token })
          .then(response => {
            expect(response.statusCode).to.be.equal(400)
          })
      })
    })
  })
})

const { getIdFromResponseLocationHeader, createModel } = require('./index.js')
const request = require('supertest')

const testUser = {
  nick: 'test_nick',
  email: 'test_email@gmail.com',
  password: 'test_pass_not_strong_but_enough'
}

const createTestUser = (app) => createModel({ model: testUser, modelType: 'user', app })

const createTestUserAndGetId = (app) =>
  createTestUser(app).then(response => getIdFromResponseLocationHeader(response))

const getUserById = ({ userId, app, token }) =>
  request(app)
    .get(`/users/${userId}`)
    .set('Authorization', `Bearer ${token}`)
const updateUserById = ({ userId, app, token, user }) =>
  request(app)
    .put(`/users/${userId}`)
    .set('Authorization', `Bearer ${token}`)
    .send(user)
const deleteUserById = ({ userId, app, token }) =>
  request(app)
    .delete(`/users/${userId}`)
    .set('Authorization', `Bearer ${token}`)
const getUserComments = ({ userId, app, token }) =>
  request(app)
    .get(`/users/${userId}/comments`)
    .set('Authorization', `Bearer ${token}`)
const getUsers = ({ app, token }) =>
  request(app)
    .get('/users')
    .set('Authorization', `Bearer ${token}`)

module.exports = {
  createTestUser, createTestUserAndGetId, testUser, getUserById, getUsers, getUserComments, updateUserById, deleteUserById
}

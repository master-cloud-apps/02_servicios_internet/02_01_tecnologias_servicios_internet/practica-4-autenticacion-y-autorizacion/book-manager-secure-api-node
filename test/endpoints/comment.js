const request = require('supertest')
const { createModel } = require('./index.js')

const getCommentTest = nick => ({
  content: 'Comment content for testing',
  punctuation: 3.5,
  nick
})
const createComment = ({ comment, bookId, app, token }) =>
  createModel({ model: comment, modelType: 'comment', bookId, app, token })
const deleteCommentById = ({ bookId, commentId, app, token }) =>
  request(app)
    .delete(`/books/${bookId}/comments/${commentId}`)
    .set('Authorization', `Bearer ${token}`)
const getCommentById = ({ bookId, commentId, app, token }) =>
  request(app)
    .get(`/books/${bookId}/comments/${commentId}`)
    .set('Authorization', `Bearer ${token}`)
const createTestComment = ({ app, nick, bookId, token }) =>
  createComment({ comment: getCommentTest(nick), app, bookId, token })

module.exports = {
  createComment, deleteCommentById, getCommentById, createTestComment, getCommentTest
}

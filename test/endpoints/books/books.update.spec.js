const app = require('./../../../src/app/index.js')
const { getIdFromResponseLocationHeader, manageInMemoryDatabase } = require('./../index.js')
const { expect } = require('chai')
const { createTestBook, bookTest, deleteBookById, updateBookById } = require('../book.js')
const { setupDatabase, userOne } = require('../../fixtures/db.js')
const _ = require('lodash')

describe('Book use cases', () => {
  manageInMemoryDatabase()
  beforeEach(setupDatabase)

  describe('PUT /books/:bookId use cases', () => {
    let bookTestId
    beforeEach(() => createTestBook(app, userOne.tokens[0].token)
      .then(response => getIdFromResponseLocationHeader(response))
      .then(bookId => (bookTestId = bookId)))
    it('When update by id wrong id should return 400', () => {
      return updateBookById({ bookId: '12345', app, token: userOne.tokens[0].token, book: {} })
        .then(response => expect(response.statusCode).to.equal(400))
    })
    it('Given book created, when update by id title should return 200', () => {
      const updatedBook = _.cloneDeep(bookTest)
      updatedBook.title = 'Updated Title'
      return updateBookById({ bookId: bookTestId, app, token: userOne.tokens[0].token, book: updatedBook })
        .then(response => {
          expect(response.statusCode).to.equal(200)
          expect(response.body.title).to.be.equal('Updated Title')
        })
    })
    it('Given book created, when update by id not allowed field should return 400', () => {
      const updatedBook = _.cloneDeep(bookTest)
      updatedBook.invalidFieldToUpdate = 'Updated Title'
      return updateBookById({ bookId: bookTestId, app, token: userOne.tokens[0].token, book: updatedBook })
        .then(response => {
          expect(response.statusCode).to.equal(400)
          expect(response.body.error).to.be.equal('Invalid update field')
        })
    })
    it('Given book deleted, when update by id should return not found', () => {
      const updatedBook = _.cloneDeep(bookTest)
      updatedBook.invalidFieldToUpdate = 'Updated Title'
      return deleteBookById({ bookId: bookTestId, app, token: userOne.tokens[0].token })
        .then(response => expect(response.statusCode).to.be.equal(204))
        .then(_ => updateBookById({ bookId: bookTestId, app, token: userOne.tokens[0].token, book: {} }))
        .then(response => expect(response.statusCode).to.equal(404))
    })
  })
})

const app = require('./../../../src/app/index.js')
const { getIdFromResponseLocationHeader, manageInMemoryDatabase } = require('./../index.js')
const { expect } = require('chai')
const { createTestBook, deleteBookById, getBookById } = require('../book.js')
const { setupDatabase, userOne } = require('../../fixtures/db.js')

describe('Book use cases', () => {
  manageInMemoryDatabase()
  beforeEach(setupDatabase)

  describe('DELETE /books/:bookId use cases', () => {
    let bookTestId
    beforeEach(() => createTestBook(app, userOne.tokens[0].token)
      .then(response => getIdFromResponseLocationHeader(response))
      .then(bookId => (bookTestId = bookId)))
    it('Given book created, when delete by id should return 204', () => {
      return deleteBookById({ bookId: bookTestId, app, token: userOne.tokens[0].token })
        .then(response => expect(response.statusCode).to.equal(204))
    })
    it('Given book created, when delete by id wrong id should return bad request', () => {
      return deleteBookById({ bookId: '12345', app, token: userOne.tokens[0].token })
        .then(response => expect(response.statusCode).to.equal(400))
    })
    it('Given created book when delete book then get book by id should return 404', () => {
      return getBookById({ bookId: bookTestId, app, token: userOne.tokens[0].token })
        .then(response => expect(response.statusCode).to.be.equal(200))
        .then(_ => deleteBookById({ bookId: bookTestId, app, token: userOne.tokens[0].token }))
        .then(response => expect(response.statusCode).to.be.equal(204))
        .then(_ => getBookById({ bookId: bookTestId, app, token: userOne.tokens[0].token }))
        .then(response => expect(response.statusCode).to.be.equal(404))
    })
  })
})

const { expect } = require('chai')
const jwt = require('jsonwebtoken')
const sinon = require('sinon')
const { setRole } = require('../../src/middlewares/auth/index.js')

describe('Set role handler use cases', () => {
  it('When jwt token sent, should include token in the request', () => {
    const spyNext = sinon.spy()
    const req = (() => {
      const headers = {}
      return {
        header: headerName => headers[headerName],
        setHeader: (headerName, headerValue) => (headers[headerName] = headerValue)
      }
    })()
    const token = jwt.sign({ _id: 12345 }, process.env.JWT_SECRET)
    req.setHeader('Authorization', `Bearer ${token}`)
    setRole(req, {}, spyNext)
    expect(req.token).to.be.equal(token)
    expect(req.role).to.be.equal('REGISTERED')
    expect(spyNext.calledOnce).to.be.equal(true)
  })
  it('When jwt token not valid sent, should not include token in the request', () => {
    const spyNext = sinon.spy()
    const req = (() => {
      const headers = {}
      return {
        header: headerName => headers[headerName],
        setHeader: (headerName, headerValue) => (headers[headerName] = headerValue)
      }
    })()
    const token = 12345
    req.setHeader('Authorization', `Bearer ${token}`)
    setRole(req, {}, spyNext)
    expect(req.token).to.be.equal(undefined)
    expect(req.role).to.be.equal('NOT_REGISTERED')
    expect(spyNext.calledOnce).to.be.equal(true)
  })
  it('When not jwt token sent, should not include token in the request', () => {
    const spyNext = sinon.spy()
    const req = (() => {
      const headers = {}
      return {
        header: headerName => headers[headerName],
        setHeader: (headerName, headerValue) => (headers[headerName] = headerValue)
      }
    })()
    setRole(req, {}, spyNext)
    expect(req.token).to.be.equal(undefined)
    expect(req.role).to.be.equal('NOT_REGISTERED')
    expect(spyNext.calledOnce).to.be.equal(true)
  })
})
